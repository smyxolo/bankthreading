import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Bank {

    private List<BankAccount> accountList;

    public List<BankAccount> getAccountList() {
        return accountList;
    }

    public Bank() {
        accountList = new ArrayList<>();
        accountList.add(new BankAccount(0));
    }

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    public void sendSubtractRequest (double howMuch){
        BankRequest br = new BankRequest(accountList.get(0), RequestType.SUB, howMuch);
        executorService.submit(br);
    }

    public void sendAddToAccountRequest (double howMuch){
        BankRequest br = new BankRequest(accountList.get(0), RequestType.ADD, howMuch);
        executorService.submit(br);
    }

    public void printBalanceRequest(){
        BankRequest br = new BankRequest(accountList.get(0), RequestType.BAL);
    }
}
