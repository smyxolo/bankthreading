import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BankTest {

    Bank bank;
    int quote;

    @Before
    public void setUp() throws Exception {
        bank = new Bank();
        quote = 100;
    }

    @Test
    public void sendSubtractRequest() {
        bank.sendSubtractRequest(100d);
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(-100d, bank.getAccountList().get(0).getSaldo(), 0.001d);
    }

    @Test
    public void sendAddToAccountRequest() {
        bank.sendAddToAccountRequest(100d);
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(100d, bank.getAccountList().get(0).getSaldo(), 0.001d);
    }

    @Test
    public void printBalanceRequest() {
    }

    @Test
    public void thousandOppositeTransactionsGivesZero(){
        for (int i = 0; i < 1000; i++) {
            bank.sendAddToAccountRequest(100);
            bank.sendSubtractRequest(100);
        }

        try {
            Thread.sleep(1000*600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(0d, bank.getAccountList().get(0).getSaldo(), 0.001d);
    }
}