public class BankAccount {
    private double saldo = 0;

    public BankAccount(double saldo) {
        this.saldo = saldo;
    }

    public BankAccount() {
    }

    public double getSaldo() {
        return saldo;
    }

    public void balance(){
        System.out.println("saldo = " + saldo);
    }

    public void add(double ile){
        saldo += ile;
    }

    public void sub(double ile){
        saldo -= ile;
    }
}
