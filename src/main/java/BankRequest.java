import java.util.Optional;

public class BankRequest implements Runnable {
    private BankAccount bankAccount;
    private RequestType requestType;
    private double quote;

    public BankRequest(BankAccount bankAccount, RequestType requestType) {
        this.bankAccount = bankAccount;
        this.requestType = requestType;
    }

    public BankRequest(BankAccount bankAccount, RequestType requestType, double quote) {
        this.bankAccount = bankAccount;
        this.requestType = requestType;
        this.quote = quote;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        switch (requestType){
            case ADD: {
                bankAccount.add(quote);
                break;
            }
            case SUB: {
                bankAccount.sub(quote);
                break;
            }
            case BAL: {
                bankAccount.balance();
                break;
            }
        }
    }
}
